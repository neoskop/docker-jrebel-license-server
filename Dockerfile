FROM openjdk:8u171-jre-stretch
LABEL maintainer "devops@neoskop.de"
ENV VERSION="3.5.2"
RUN mkdir /jrebel && \
    useradd -ms /bin/bash docker && \
    wget -O /tmp/license-server.zip -q "http://dl.zeroturnaround.com/license-server/license-server-${VERSION}.zip" && \
    unzip -u -o /tmp/license-server.zip -d /jrebel && \
    rm /tmp/license-server.zip && \
    mkdir /jrebel/license-server/data && \
    chown -R docker:docker /jrebel
COPY docker-entrypoint.sh /
EXPOSE 9000
VOLUME ["/jrebel/license-server/data"]
ENTRYPOINT [ "/docker-entrypoint.sh" ]